#!/usr/bin/env ruby

# A better version of the original branching script written by Karan <karan@gwynniebee.com>
#
# Assumptions:
#   1. versioning scheme is <major>.<minor>.<patchlevel>
#   2. branches are always <major>.<minor>
#   3. <patchlevel> is always only updated in branches
#   4. All projects have pom.xml at the root of trunk,
#      and that is the source of truth as far as version
#      is concerned
#   5. The developer has his trunk up to date
#   6. Developer will take care of the "configuration"
#      release part
#
# Workflow:
#   1. Create branch
#   2. Checkout newly created branch
#   3. Remove -SNAPSHOT from branch POMs
#   4. Commit branch changes
#   5. Give an option for updating and committing svn:externals
#   6. Bump trunk's major or minor version respectively
#
# TODOs:
#   * Unification w.r.t child output, log and interactive messages
#   * When we spit parsed XML out, the order of attributes may change
#     Need a fix for this
#   * Currently we don't check whether a new configuration would also
#     be released as part of the operation or not. There's no sane way
#     of detecting that unless we assume that the process is strictly
#     followed and based on that we can expect older release branch to be
#     there. Let's see what can we do about this
#
# Author: Pradeep Jindal <pradeep@gwynniebee.com>

require 'optparse'
require 'ostruct'
require 'logger'
require 'uri'
require 'tempfile'
require 'rexml/document'

options = OpenStruct.new
log = Logger.new(STDOUT)
log.level = Logger::INFO
options.log = log

def show_svn_diff(options, cmd_args)
  out = run_command(options, "svn diff #{cmd_args}")
  unless out
    return false
  end
  out.split("\n").each do |line|
    options.log.info("SVNDIFF: #{line}")
  end
  return true
end

def svn_url_from_dir(options, dir)
  cmd = "svn info #{dir} 2>&1"
  out = `#{cmd}`
  if $?.exitstatus != 0
    options.log.error("\"#{cmd}\" exited with non-zero exit code: #{out}")
    return nil
  else
    m = out.match(/^URL:\s+(.+)$/)
    if m.captures.length != 1
      options.log.error("Couldn't extract information from \"#{cmd}\"'s output: #{out}")
      return nil
    end
    return m.captures[0]
  end
end

def question?(options, ques, default=nil, possible_ans=[])
  unless options.interactive
    options.log.info("[NON-INTERACTIVE] #{ques} (#{possible_ans.join(",")}) [#{default}] #{default}")
    return default
  end
  possible_ans_default = possible_ans + [""]
  # bad hack: using space character instead of nil because
  # the while statement below would assume the default if
  # ans is set to nil because nil's string representation is
  # "" and that's what we expect for choosing the default
  ans = " "
  while possible_ans_default.grep(/^#{ans}$/).length <= 0
    puts "Wrong answer, please choose from these: #{possible_ans.join(",")}" unless ans =~ / /
    print "#{ques} (#{possible_ans.join(",")}) [#{default}] "
    ans = gets.chomp!
  end
  if ans.length > 0
    return ans
  else
    return default
  end
end

def question_yn?(options, ques, default="y")
  ans = question?(options, ques, default=default, possible_ans=["y","n"])
  if ans == "y"
    return true
  else
    return false
  end
end

def should_we_proceed?(options, ques, default="y")
  if question_yn?(options, ques, default)
    return true
  else
    exit 1
  end
end

def run_command(options, cmd)
  options.log.info("Running command \"#{cmd}\"")
  out = `#{cmd} 2>&1`
  if $?.exitstatus != 0 or out.nil?
    options.log.warn("\"#{cmd}\" exited with non-zero exit code, here's the command output:\n#{out}")
    return false
  end
  return out
end

def parsed_pom(options, file)
  begin
    return REXML::Document.new(File.read(file))
  rescue => e
    options.log.error("Couldn't parse POM XML \"#{file}\": \n#{e.message}")
    return nil
  end
end

# returns version with -SNAPSHOT stripped out
def get_version_from_pom(options, file)
  pom = parsed_pom(options, file)
  if pom.nil?
    options.log.error("Couldn't identify the version to branch, exiting...")
    return nil
  end
  elements = pom.get_elements("/project/version")
  if elements.length < 1
    options.log.error("Parsed trunk/pom.xml either isn't a valid pom or doesn't contain the version element")
    return nil
  end
  return elements[0].text.sub(/(.+)-SNAPSHOT$/, '\1')
end

def write_file_atomic(options, data, dest)
  tmp_file = Tempfile.new('generateBranchPom', File.dirname(dest))
  begin
    tmp_file.write(data)
    tmp_file.close
    File.rename(tmp_file.path, dest)
  rescue => e
    options.log.error("Couldn't write to file #{dest} atomtically: #{e.message}")
    return false
  ensure
    tmp_file.close unless tmp_file.closed?
    tmp_file.unlink if File.exists?(tmp_file.path)
  end
  return true
end

OptionParser.new do |opts|
  opts.banner = "Usage: generateBranch.rb [options]"

  # XXX It just sucks the way we have structured our projects because it makes the
  #     release process all very complex and tricky to automate sanely.
  opts.on("-N", "--nonglobal-snap-removal", "[BAD HACK] by default we remove -SNAPSHOT from all the pom.xml files found in the branch, use this flag to disable this behaviour and only operate on the files specified with -f") do |p|
    options.nonglobal_snap_removal = true
  end

  opts.on("-f", "--pom-files LIST", "comma separated list of pom file paths (relative to trunk or branch) that we should operate on when doing -SNAPSHOT removal or version bump, see --nonglobal-snap-removal also, default is \"pom.xml,assembly/pom.xml\"") do |f|
    options.pom_files = f
  end

  opts.on("-P", "--production-dir PATH", "which production dir should we change the svn:externals property on, this path is relative to the project dir, default is ./production") do |p|
    options.production_dir = p
  end

  opts.on("-b", "--branch-version VERSION", "Version number to use for the branch, if not specified we will try to figure out from the pom.xml in trunk, e.g. 1.2") do |v|
    options.project_version = v
  end

  opts.on("-o", "--only-version", "while removing -SNAPSHOT operate only on the project version, by default we operate on the whole pom") do |o|
    options.only_version = true
  end

  opts.on("-j", "--jira-ticket TICKET", "JIRA ticket to use for committing changes, e.g. INFRA-123") do |j|
    options.jira_ticket = j
  end

  opts.on("-m", "--bump-major", "By default we bump the minor version of trunk after branching, use this if you want to bump the major version") do |m|
    options.bump_major = true
  end

  opts.on("-i", "--interactive", "Should the program be interactive, default is to be non-interactive") do |i|
    options.interactive = true
  end

  opts.on("-l", "--log-level LEVEL", [:FATAL, :ERROR, :WARN, :INFO, :DEBUG], "Set the log level for the program run, default is INFO") do |l|
    options.log.level = Logger.const_get(l)
  end

  opts.on("-p", "--project PATH", "PATH where the svn project had been checked out, default is the current directory") do |p|
    if File.directory?(p)
      options.project_dir = p
      options.project_url = svn_url_from_dir(options, options.project_dir)
      if options.project_url.nil?
        log.error("Couldn't identify svn repo for project dir #{options.project_dir}")
        exit 1
      end
    else
      log.error("Specified project \"#{p}\" is either not a dir or doesn't exist, exiting...")
      exit 1
    end
  end

end.parse!

if options.jira_ticket.nil?
  log.error("Jira Ticket to use must be specified")
  exit 1
end

if options.project_url.nil?
  options.project_dir = Dir.pwd
  options.project_url = svn_url_from_dir(options, options.project_dir)
  if options.project_url.nil?
    log.error("Couldn't identify svn repo for current dir")
    exit 1
  end
end

if options.pom_files.nil?
  options.pom_files = "pom.xml,assembly/pom.xml"
end
options.pom_files_l = options.pom_files.split(",")

if options.production_dir.nil?
  options.production_dir = "./production"
end

unless run_command(options, "svn ls --depth empty #{options.project_url}/trunk #{options.project_url}/branches #{options.project_url}/trunk/pom.xml")
  log.error("Identified project \"#{options.project_url}\" doesn't either have a trunk or trunk/pom.xml or branches")
  exit 1
end

unless File.exists?("#{options.project_dir}/trunk")
  log.warn("#{options.project_url}/trunk isn't checked out to #{options.project_dir}/trunk")
  if should_we_proceed?(options, "Should we check it out?")
    log.info("Attempting to checkout trunk")
    unless run_command(options, "svn co #{options.project_url}/trunk #{options.project_dir}/trunk")
      log.error("Couldn't check the trunk out, exiting...")
      exit 1
    end
  end
end

options.orig_pwd = Dir.pwd
Dir.chdir(options.project_dir)

if options.project_version.nil?
  should_we_proceed?(options, "Branch version isn't specified on the command line, should we try to figure it out from trunk/pom.xml?")
  options.project_version = get_version_from_pom(options, "trunk/pom.xml")
end

# Figure major, minor and patchlevel from version string
options.project_major, options.project_minor, options.project_patchlevel = options.project_version.split(".")

if options.project_major.nil? or options.project_minor.nil?
  log.error("Branch version should be in this format: <major>.<minor>[.<patchlevel>][-SNAPSHOT]")
  exit 1
end

# Form a branch version for later use, note that we don't use patchlevel while
# branching things out, patchlevels are bump in the branch itself in case of 
# bugfixes and stuff
options.branch_version = "#{options.project_major}.#{options.project_minor}"
log.info("We figured that the version to be branched is: #{options.branch_version}")


unless File.exists?("#{options.project_dir}/branches")
  log.error("#{options.project_url}/branches isn't checked out to #{options.project_dir}/branches")
  if should_we_proceed?(options, "Should we check it out?")
    log.info("Attempting to checkout branches, just the empty directory")
    unless run_command(options, "svn co --depth empty #{options.project_url}/branches #{options.project_dir}/branches")
      log.error("Couldn't check the branches out, exiting...")
      exit 1
    end
  end
end

# Create branch

if run_command(options, "svn ls --depth empty #{options.project_url}/branches/#{options.branch_version}")
  unless question_yn?(options, "Branch \"#{options.project_url}/branches/#{options.branch_version}\" already exists, should we proceed with the rest of the steps?")
    exit 1
  end
else
  should_we_proceed?(options, "We are now going to create a branch, should we proceed?")
  unless run_command(options, "svn cp -m '#{options.jira_ticket} - branching things out' #{options.project_url}/trunk #{options.project_url}/branches/#{options.branch_version}")
    log.error("Couldn't branch things out, exiting...")
    exit 1
  end
end

# Checkout the created branch

if File.exists?("branches/#{options.branch_version}")
  unless question_yn?(options, "Branch is already checked out, please make sure its up to date before proceeding, should we proceed with the rest of the steps?")
    exit 1
  end
else
  should_we_proceed?(options, "We are now going to checkout the newly created branch, should we proceed?")
  unless run_command(options, "svn co #{options.project_url}/branches/#{options.branch_version} branches/#{options.branch_version}")
    log.error("Couldn't checkout the newly created branch, exiting...")
    exit 1
  end
end

# Remove -SNAPSHOT

should_we_proceed?(options, "We are now going to remove '-SNAPSHOT' from the checked out branch, should we proceed?")

branch_pom_files_l = []
if options.nonglobal_snap_removal
  branch_pom_files_l = options.pom_files_l.map {|x| "branches/#{options.branch_version}/#{x}"}
else
  branch_pom_files_l = Dir.glob("branches/#{options.branch_version}/**/pom.xml")
  log.info("HIT HERE : #{branch_pom_files_l.join(",")}")
end

branch_pom_files_l.each do |pom_file|
  pom_wo_snap = nil
  unless options.only_version
    pom_wo_snap = File.read(pom_file).gsub(/(.+)-SNAPSHOT(.*)/, '\1\2')
  else
    pom = parsed_pom(options, pom_file)
    pom.get_elements("/project/version")[0].text = "#{options.project_version}"
    pom_wo_snap = pom.to_s
  end

  unless pom_wo_snap.nil?
    unless write_file_atomic(options, pom_wo_snap, pom_file)
      log.error("Couldn't write modified pom to \"#{pom_file}\"")
      exit 1
    end
  else
    log.error("Couldn't remove -SNAPSHOT from #{pom_file}")
    exit 1
  end
end

# Commit branch changes

branch_pom_files = branch_pom_files_l.join(" ")
unless show_svn_diff(options, branch_pom_files)
  log.error("Couldn't diff the branch pom changes")
  exit 1
end
should_we_proceed?(options, "We are now going to commit branch changes, should we proceed?")
unless run_command(options, "svn ci -m '#{options.jira_ticket} - removing -SNAPSHOT from the branch' #{branch_pom_files}")
  log.error("Couldn't commit branch changes, exiting...")
  exit 1
end

# Update svn:externals

if question_yn?(options, "Now we are going to give you the option of changing svn:externals, do you want to do it right now?")
  unless system("svn pe svn:externals #{options.production_dir}")
    log.error("There was some problem while updating the svn:externals on #{options.production_dir}, exiting...")
    exit 1
  end
end

# Commit svn:externals changes
if run_command(options, "svn st #{options.production_dir} | grep -E '^[ ]*(M|A)'")
  unless show_svn_diff(options, "#{options.production_dir}")
    log.error("Couldn't diff the #{options.production_dir} changes")
    exit 1
  end
  if question_yn?(options, "There are changes to committed in #{options.production_dir}, they are probably the svn:externals ones, should we commit them?")
    unless run_command(options, "svn ci -m '#{options.jira_ticket} - updating svn:externals' #{options.production_dir}")
      log.error("Couldn't commit svn:externals changes on #{options.production_dir}, exiting...")
      exit 1
    end
  end
end

# Bump the trunk version

should_we_proceed?(options, "We are now going to bump trunk's \"#{options.bump_major ? "major" : "minor"}\" version in [#{options.pom_files_l.join(",")}], should we proceed?")

if options.bump_major
  options.project_major = options.project_major.to_i.next.to_s
else
  options.project_minor = options.project_minor.to_i.next.to_s
end

options.bumped_trunk_version = "#{options.project_major}.#{options.project_minor}" + (options.project_patchlevel ? ".#{options.project_patchlevel}" : "") + "-SNAPSHOT"

options.pom_files_l.each do |pom_file|
  pom = parsed_pom(options, "trunk/#{pom_file}")
  pom.get_elements("/project/version")[0].text = "#{options.bumped_trunk_version}"
  unless write_file_atomic(options, pom.to_s, "trunk/#{pom_file}")
    log.error("Couldn't write modified pom to trunk/#{pom_file}")
    exit 1
  end
end
log.info("Below are the pom changes in trunk")
unless show_svn_diff(options, options.pom_files_l.map {|x| "trunk/#{x}"}.join(" "))
  log.error("Couldn't diff the trunk pom changes")
  exit 1
end
